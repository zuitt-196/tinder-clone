import React, { useState, useEffect } from 'react';
import TinderCard from 'react-tinder-card';
import './TenderCasrd.css';
import axios from './axios'


const TenderCasrd = () => {
    const [peoples, setPeoples] = useState([]);

    useEffect(() => {
        async function fetchData() {
            const req = await axios.get('/tinder/cards');
            setPeoples(req.data);
        }
        fetchData();
    }, [])

    console.log(peoples)
    const swiped = (direction, nameToDelete) => {
        console.log('removing: ' + nameToDelete)
    }

    const outOfFrame = (name) => {
        console.log(name + ' left the screen')
    }

    return (
        <div className="tenderCards">
            <div className="tinderCards__cardContainer">
                {peoples.map((person) => (

                    <TinderCard className="swipe"
                        key={person.name}
                        preventSwipe={["up", "down"]}
                        onSwipe={(direction) => swiped(direction, person.name)}
                        onCardLeftScreen={() => outOfFrame(person.name)}
                    >
                        <div style={{ backgroundImage: `url(${person.imgUrl})` }} className="card" >
                            <h3>{person.name}</h3>
                        </div>
                    </TinderCard>

                ))}
            </div>


        </div>
    )
}

export default TenderCasrd
