import React from 'react'
import './Header.css'
import PersonIcon from '@mui/icons-material/Person';
import IconButton from '@mui/material/IconButton';
import ImageTender from './images/tender-removebg-preview.png'
import ForumIcon from '@mui/icons-material/Forum';
const Header = () => {
    return (
        <div className='header'>
            <IconButton>
                <PersonIcon fontSize="large" className="header_icon" />
            </IconButton>
            {/* Tender logo */}
            <img className='header_logo' src={ImageTender} alt="" />

            <IconButton>
                <ForumIcon fontSize="large" className="header_icon"/>
            </IconButton>
        </div>
    )
}

export default Header
