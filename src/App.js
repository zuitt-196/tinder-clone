
import './App.css';
import Header from './Header';
import TenderCasrd from './TenderCasrd';
import SwipeButton from './SwipeButton'

function App() {
  return (
    <div className="app">



      {/* Header */}
      <Header />

      {/* Tender Cards */}
      <TenderCasrd />

      {/* swipe buttom */}
      <SwipeButton />
    </div>
  );
}

export default App;
